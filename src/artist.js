/*
 * Copyright 2020 Kavan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

const { GObject, Gtk, Gdk, Gio, GdkPixbuf } = imports.gi;
const Album = imports.album.Album;

var Artist = GObject.registerClass({
    GTypeName: 'Artist',
    Template: 'resource:///com/kavan/MusicPlayer/ui/artist.ui',
    InternalChildren: ['artistPic', 'artistName']
}, class Artist extends Gtk.Box {
    _init(name) {
        super._init({});
        this._artistName.label = name;

        this._artistPic.pixbuf = GdkPixbuf.Pixbuf.new_from_resource('/com/kavan/MusicPlayer/assets/artist-symbolic.svg');
        this._artistPic.radius = 35;
    }
});