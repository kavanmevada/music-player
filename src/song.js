/*
 * Copyright 2020 Kavan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

const { GObject, Gtk, Gst, Gdk, Gio, GdkPixbuf } = imports.gi;
const Utils = imports.utils.Utils;

var Song = GObject.registerClass({
    GTypeName: 'Song',
    Template: 'resource:///com/kavan/MusicPlayer/ui/song.ui',
    InternalChildren: ['title', 'artist', 'album', 'duration', 'rowEventBox', 'playButton', 'playbackStack', 'songCount', 'songArt'],
    Signals: {
        'play': {},
    },
    CssName: 'song-item',
}, class Song extends Gtk.ListBoxRow {
    _init(info) {
        super._init({}); 
        this.info = info;

        this._title.label = this.info.title
        this._artist.label = this.info.artist

        this.show_album = false;

        this._duration.label = Utils.formatTime(this.info.duration / Gst.SECOND);

        this._artworkPix = this.info.artwork;
        this._songCount.label = this.info.trackNum.toString();

        this._rowEventBox.add_events(Gdk.EventMask.BUTTON_PRESS_MASK | Gdk.EventMask.BUTTON_RELEASE_MASK | Gdk.EventMask.LEAVE_NOTIFY_MASK | Gdk.EventMask.ENTER_NOTIFY_MASK);
        this._rowEventBox.connect('button-press-event', () => this.emit('play'));

        this._rowEventBox.connect('enter-notify-event', () => {
            this._playButton.show();
        })
        this._rowEventBox.connect('leave-notify-event', () => {
            this._playButton.hide();
        })
    }

    set show_numbers(bool) {
        if (bool) {
            this._playbackStack.visible_child_name = 'index';
        } else {
            this._playbackStack.visible_child_name = 'image';
            this._songArt.radius = 3;
            this._songArt.pixbuf = this._artworkPix;
        }
    }

    set show_album(bool) {
        if (bool) {
            this._album.show();
            this._album.label = this.info.album;
        } else {
            this._album.hide();
        }
    }
});