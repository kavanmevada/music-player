var Utils = {
    get_albums: (list) => {
        let albumsMap = new Map();
        list.forEach((song, index) => {
            let _album = albumsMap.get(song.album)
            if (!_album) {
                albumsMap.set(song.album, [index])
            } else {
                _album.push(index)
            }
        });

        // Debug
        // -------------
        // let albums = Utils.get_albums(this.foundList);
        // for (const [key, value] of albums.entries()) {
        //     log(`${key} => ${value}`);
        // }

        return albumsMap;
    },

    get_artists: (list) => {
        let artistsMap = new Map();
        list.forEach((song, index) => {
            let seperated_artists = song.artist.split(/, | & /);
            seperated_artists.forEach(_artist => {
                let _found_artist = artistsMap.get(_artist);
                if (!_found_artist) {
                    let _new = new Map();
                    _new.set(song.album, [index]);
                    artistsMap.set(_artist, _new);
                } else {
                    let _found_album = _found_artist.get(song.album);
                    if (_found_album) {
                        _found_album.push(index);
                    } else {
                        _found_artist.set(song.album, [index]);
                    }
                }
            });
        });

        // Debug:
        // --------------
        // let artist = Utils.get_artists(this.foundList);
        // for (const [key, value] of artist.entries()) {
        //     for (const [_key, _value] of value.entries()) {
        //         log(`Artist: ${key} => ${_key} => ${_value}`);
        //     }
        // }


        return artistsMap;
    },


    get_artwork: (discovererInfo) => {
        let _tags = discovererInfo.get_tags();
        let file = Gio.File.new_for_path (this.album);
        if (!file.query_exists(null)) {
            var imageHex = /image=\(sample\)(.+?):/g.exec(_tags.to_string())[1];
            var typedArray = new Uint8Array(imageHex.match(/.{1,2}/g).map(byte => parseInt(byte, 16)));

            let ios = file.create_readwrite(Gio.FileCreateFlags.PRIVATE, null);
            let stream = ios.get_output_stream();
            stream.write_all(typedArray, null);
        }
        return GdkPixbuf.Pixbuf.new_from_file(this.album);
    },


    formatTime: unformattedTime => {
        const _TIME_DIVISOR = 60;
        this.unformattedTime = unformattedTime;
        let seconds = Math.floor(this.unformattedTime);
        let hours = parseInt(seconds / Math.pow(_TIME_DIVISOR, 2));
        let hoursString = '';

        if (hours > 10)
            hoursString = `${hours}:`;
        else if (hours < 10 && hours > 0)
            hoursString = `0${hours}:`;

        let minuteString = parseInt(seconds / _TIME_DIVISOR) % _TIME_DIVISOR;
        let secondString = parseInt(seconds % _TIME_DIVISOR);
        let timeString = `${hoursString + (minuteString < 10 ? `0${minuteString}` : minuteString)}:${secondString < 10 ? `0${secondString}` : secondString}`;

        return timeString;
    },
}