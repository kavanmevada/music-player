/*
 * Copyright 2020 Kavan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

const { GObject, Gst } = imports.gi;


var Player = class Player {
    constructor() {
        this._player = Gst.ElementFactory.make('playbin', 'play');
        this._player.set_property('audio-sink', 
            Gst.ElementFactory.make('pulsesink', 'sink'));

        this._playerBus = this._player.get_bus();
        this._playerBus.connect('message', (playerBus, message) => {
            if (message !== null)
                this._onMessageReceived(message);
        });

        this._playQueue = null;
        this._index = 0;
        this.volume = 1.0;
    }

    get state() {
        return this._state;
    }

    set state(state) {
        this._state = state;
        this._player.set_state(this._state);
    }

    get volume() {
        return this._volume;
    }

    set volume(volume) {
        // Set the volume level to a value in the range [0, 1.5]
        this._volume = volume
        this._player.set_property("volume", this._volume)
    }

    get position() {
        return this._player.query_position(Gst.Format.TIME)[1];
    }

    set position(pos) {
        this._player.seek_simple(Gst.Format.TIME, Gst.SeekFlags.FLUSH, pos)
    }

    get duration() {
        return this._player.query_duration(Gst.Format.TIME)[1];
    }

    _playUri(uri) {
        this._player.set_state(Gst.State.NULL);
        this._playerBus.add_signal_watch();
        this._player.set_property('uri', uri);
        this._play();
    }

    toggelPlayback() {
        if (this.state === Gst.State.PAUSED)
            this.state = Gst.State.PLAYING;
        else if (this.state === Gst.State.PLAYING)
            this.state = Gst.State.PAUSED;
    }

    destroy() {
        this.state === Gst.State.NULL;
        this._playerBus.remove_watch();
    }

    

    _onMessageReceived(message) {
        switch (message.type) {
        case Gst.MessageType.EOS:
            this.destroy();
            break;
        case Gst.MessageType.WARNING:
            log(message.parse_warning()[0].toString());
            break;
        case Gst.MessageType.ERROR:
            this.stop();
            log(message.parse_error()[0].toString());
            break;
        }
    }
}