/*
 * Copyright 2020 Kavan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

const { GObject, Gtk, GLib, Gio, GdkPixbuf } = imports.gi;
const Player = imports.player.Player;
const Discoverer = imports.discoverer.Discoverer;
const Utils = imports.utils.Utils;
const Album = imports.album.Album;
const Artist = imports.artist.Artist;
const Song = imports.song.Song;

var MusicPlayerWindow = GObject.registerClass({
    GTypeName: 'MusicPlayerWindow',
    Template: 'resource:///com/kavan/MusicPlayer/ui/window.ui',
    InternalChildren: [ 'placessidebar', 'viewstack',
        'albumTitle',
        'albumArtist',
        'albumImg',
        'albumSongs',
        'albumBack',
        'artistBack', 'artistflowbox', 'albumsBox', 'songslist', 'artistinfoname', 'artistAlbums', 'artistSongs']
}, class MusicPlayerWindow extends Gtk.ApplicationWindow {
    _init(application) {
        super._init({ 
            application: application,
            icon_name: pkg.name,
        });

        // Initialize Player
        const _player = new Player();


        // Watch Music Directory ----------------------------
        const path = GLib.build_filenamev([GLib.get_home_dir(), _('Music')]);
        const _saveDir = Gio.file_new_for_path(path);
        const discoverer = new Discoverer(_saveDir);
        // Load List
        discoverer.connect('discovered', (_, _foundList) => this.foundList = _foundList);
        // -----------------------------------------------


        // Left sidebar control GtkStack --------------------------
        this._placessidebar.connect('row-selected', (_, row) => {
            if (row.get_index() === 0) {
                this._viewstack.visible_child_name = 'recents-page';
            } else if (row.get_index() === 1) {
                this._viewstack.visible_child_name = 'artists-page';

                let artistList = Utils.get_artists(this.foundList);
                let artistStore = new Gio.ListStore();


                for (const [name, albums] of artistList.entries()) {

                    const artist = new Artist(name);
                    const artistSongs = [];
             


                    for (const [name, songs] of albums.entries()) {
                        artistSongs.push(...songs);
                    }

                    artist.connect('button-press-event', () => {
                        this._viewstack.visible_child_name = 'artist-info-page';
                        this._artistinfoname.label = name;
                        
                        const artistSongStore = new Gio.ListStore();
                        artistSongs.forEach(songId => {
                            const songInfo = this.foundList[songId];
                            artistSongStore.append(new Song(songInfo))
                        });

                        this._artistSongs.bind_model(artistSongStore, song => {
                            song.show_numbers = true;
                            song.show_album = false;
                            return song;
                        });

            
                        const albumsStore = new Gio.ListStore();
                        for (const [name, songsIds] of albums.entries()) {
                            const songs = songsIds.map(id => this.foundList[id]);
                            albumsStore.append(new Album(name, songs))
                        }


                        this._artistAlbums.connect('size-allocate', (a, b) => {
                            b.width < 960 ? a.get_style_context().remove_class('large')
                            : a.get_style_context().add_class('large')
                        })
                        this._artistAlbums.set_selection_mode(Gtk.SelectionMode.NONE);
                        this._artistAlbums.bind_model(albumsStore, album => {
                            album.show(); return album;
                        });
                    });


                    artistStore.append(artist);
                }

                this._artistflowbox.bind_model(artistStore, artist => {
                    artist.show();
                    return artist;
                });

                this._artistBack.connect('clicked', () => {
                    this._viewstack.visible_child_name = 'artists-page';
                })

            } else if (row.get_index() === 2) {

                this._viewstack.visible_child_name = 'albums-page';

                const albumsStore = new Gio.ListStore();
                let albumInfos = Utils.get_albums(this.foundList);
                for (const [key, value] of albumInfos.entries()) {
                    const songs = value.map(id => this.foundList[id]);

                    let album = new Album(key, songs);
                    album.connect('clicked', () => {
                        this._albumTitle.label = key;
                        const name = songs[0].albumArtist;
                        this._albumArtist.label = name ? songs[0].albumArtist : 'Unknown';
                
                        this._albumImg.autoHeight = true;
                        this._albumImg.radius = 8;
                        this._albumImg.pixbuf = songs[0].artwork;
                
                        const store = new Gio.ListStore();
                        songs.forEach(song => { store.append(new Song(song)) })
                
                        this._albumSongs.bind_model(store, song => {
                            song.show_numbers = true;
                            song.show_album = false;
                            return song;
                        });

                        this._albumSongs.set_max_children_per_line(1);
                        this._albumSongs.set_selection_mode(Gtk.SelectionMode.NONE);
                        this._viewstack.visible_child_name = 'album-info-page';
                    })

                    albumsStore.append(album);
                }

                this._albumsBox.set_selection_mode(Gtk.SelectionMode.NONE);
                this._albumsBox.bind_model(albumsStore, album => {
                    album.show(); return album;
                })


                this._albumBack.connect('clicked', () => {
                    this._viewstack.visible_child_name = 'albums-page';
                })

            } else if (row.get_index() === 3) {

                const store = new Gio.ListStore();
                this.foundList.forEach(info => {
                    store.append(new Song(info));
                })

                this._songslist.bind_model(store, song => {
                    song.show_numbers = false;
                    song.show_album = true;
                    return song;
                });

                this._songslist.set_max_children_per_line(1);
                this._songslist.set_selection_mode(Gtk.SelectionMode.NONE);
                this._viewstack.visible_child_name = 'songs-page';
            }
        }) // ------------------------------------------------

    }

});