/*
 * Copyright 2020 Kavan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

const { GObject, Gtk, Gdk, Gio, GdkPixbuf } = imports.gi;
const ImageView = imports.imageview.ImageView;

var Album = GObject.registerClass({
    GTypeName: 'Album',
    Template: 'resource:///com/kavan/MusicPlayer/ui/album.ui',
    InternalChildren: ['thumbnail', 'title'],
    Signals: {
        'clicked': {},
    },
}, class Album extends Gtk.FlowBoxChild {
    _init(key, songs) {
        super._init({});
        this.connect('button-press-event', () => this.emit('clicked'));

        this._title.label = key;
        const name = songs[0].albumArtist;
        //this._albumartist.label = name ? songs[0].albumArtist : 'Unknown';
        // this._artwork.radius = 4;
        // this._artwork.autoHeight = true;
        this._thumbnail.pixbuf = songs[0].artwork;
    }
});