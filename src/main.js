/* main.js
 *
 * Copyright 2020 Kavan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

pkg.initGettext();
pkg.initFormat();
pkg.require({
  'Gio': '2.0',
  'Gtk': '3.0'
});

const { Gio, Gtk, Gst, Gdk } = imports.gi;

const { MusicPlayerWindow } = imports.mainWindow;

function main(argv) {
    const application = new Gtk.Application({
        application_id: 'com.kavan.MusicPlayer',
        flags: Gio.ApplicationFlags.FLAGS_NONE,
    });

    application.connect('activate', app => {
        let activeWindow = app.activeWindow;
        loadStyleSheet();
        
        if (!activeWindow) {
            // Initialize GStreamer
            Gst.init(null);
            
            activeWindow = new MusicPlayerWindow(app);
        }

        activeWindow.present();
    });

    return application.run(argv);
};

// Load CSS StyleSheet
function loadStyleSheet() {
    let cssProvider = new Gtk.CssProvider();
        cssProvider.load_from_resource('/com/kavan/MusicPlayer/assets/stylesheet.css');
        Gtk.StyleContext.add_provider_for_screen(Gdk.Screen.get_default(),
            cssProvider,
            Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);
}
