/*
 * Copyright 2020 Kavan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

const { GObject, Gtk, Gio, GLib, GstPbutils, GdkPixbuf } = imports.gi;
const Signals = imports.signals;


var Discoverer = class Discoverer {
    constructor(directory) {
        directory.enumerate_children_async('standard::name',
            Gio.FileQueryInfoFlags.NOFOLLOW_SYMLINKS,
            GLib.PRIORITY_LOW,
            null,
            (obj, res) => {
                this._enumerator = obj.enumerate_children_finish(res);
                if (this._enumerator !== null)
                    this._dirEnumerator(directory);
                else
                    log('The contents of the Recordings directory were not indexed.');
        });

        this._foundList = [];
    }

    _dirEnumerator(directory) {
        this._enumerator.next_files_async(20, GLib.PRIORITY_DEFAULT, null, (_obj, _res) => {
            let fileInfos = _obj.next_files_finish(_res);
            var discoverer = new GstPbutils.Discoverer();
            discoverer.start();

            if (fileInfos.length > 0) {
                fileInfos.forEach(info => {
                    let path = GLib.build_filenamev([directory.get_path(), info.get_name()]);
                    let file = Gio.file_new_for_path(path);
                    discoverer.discover_uri_async(file.get_uri());
                });

                discoverer.connect('discovered', (_discoverer, audioInfo) => {
                    //const songItem = new Song(audioInfo);

                    let _tags = audioInfo.get_tags();
                    let _albumName = _tags.get_string('album')[1];

                    this._foundList.push({
                        uri: audioInfo.get_uri(),
                        title: _tags.get_string('title')[1],
                        album: _albumName,
                        albumArtist: _tags.get_string('album-artist')[1],
                        artist: _tags.get_string('artist')[1],
                        artwork: this.artwork(_tags, _albumName),
                        trackNum: _tags.get_uint('track-number')[1],
                        composer: _tags.get_string('composer')[1],
                        genre: _tags.get_string('genre')[1],
                        duration: audioInfo.get_duration(),
                        copyright: _tags.get_string('copyright')[1],
                    });

                    if (this._foundList.length === fileInfos.length)
                        this.emit('discovered', this._foundList);
                });

            } else {
                this._enumerator.close(null);
            }
        });
    }


    artwork(tags, albumName) {
        let file = Gio.File.new_for_path (albumName);
        if (!file.query_exists(null)) {
            var imageHex = /image=\(sample\)(.+?):/g.exec(tags.to_string())[1];
            var typedArray = new Uint8Array(imageHex.match(/.{1,2}/g).map(byte => parseInt(byte, 16)));

            // const pix = GdkPixbuf.Pixbuf.new_from_bytes(typedArray, GdkPixbuf.Colorspace.RGB, false, 8, 600, 300, 600 * 4);
            // log(pix)

            let ios = file.create_readwrite(Gio.FileCreateFlags.PRIVATE, null);
            let stream = ios.get_output_stream();
            stream.write_all(typedArray, null);
        }
        
        return GdkPixbuf.Pixbuf.new_from_file(albumName);
    }
};

Signals.addSignalMethods(Discoverer.prototype);