/*
 * Copyright 2020 Kavan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

const { GObject, Gtk, Gdk, Gio, GdkPixbuf } = imports.gi;

var ImageView = GObject.registerClass({
    GTypeName: 'ImageView',
}, class ImageView extends Gtk.DrawingArea {
    _init(param={}) {
        super._init(param);
        this.pixbuf = null;
    }
    
    vfunc_draw(ctx) {
        if (this.pixbuf) {
            const allocW = this.get_allocation().width;
            const tmpScaled = this.pixbuf.scale_simple(allocW, allocW, GdkPixbuf.InterpType.BILINEAR);
            Gdk.cairo_set_source_pixbuf(ctx, tmpScaled, 0, 0);
            ctx.paint();
        }
    }




    vfunc_get_request_mode() {
		return Gtk.SizeRequestMode.HEIGHT_FOR_WIDTH;
    }


    vfunc_get_preferred_height_for_width(width) {
        return [width, width];
    }

    // debugSignals() {
    //     let signals = [
    //     "accel-closures-changed",
    //     "button-press-event",
    //     "button-release-event",
    //     "can-activate-accel",
    //     "child-notify",
    //     "composited-changed",
    //     "configure-event",
    //     "damage-event",
    //     "delete-event",
    //     "destroy",
    //     "destroy-event",
    //     "direction-changed",
    //     "drag-begin",
    //     "drag-data-delete",
    //     "drag-data-get",
    //     "drag-data-received",
    //     "drag-drop",
    //     "drag-end",
    //     "drag-failed",
    //     "drag-leave",
    //     "drag-motion",
    //     //"draw",
    //     "enter-notify-event",
    //     "event",
    //     "event-after",
    //     "focus",
    //     "focus-in-event",
    //     "focus-out-event",
    //     "grab-broken-event",
    //     "grab-focus",
    //     "grab-notify",
    //     "hide",
    //     "hierarchy-changed",
    //     "key-press-event",
    //     "key-release-event",
    //     "keynav-failed",
    //     "leave-notify-event",
    //     "map",
    //     "map-event",
    //     "mnemonic-activate",
    //     "motion-notify-event",
    //     "move-focus",
    //     "parent-set",
    //     "popup-menu",
    //     "property-notify-event",
    //     "proximity-in-event",
    //     "proximity-out-event",
    //     "query-tooltip",
    //     "realize",
    //     "screen-changed",
    //     "scroll-event",
    //     "selection-clear-event",
    //     "selection-get",
    //     "selection-notify-event",
    //     "selection-received",
    //     "selection-request-event",
    //     "show",
    //     "show-help",
    //     "size-allocate",
    //     //"state-changed",
    //     //"state-flags-changed",
    //     //"style-set",
    //     //"style-updated",
    //     "touch-event",
    //     "unmap",
    //     "unmap-event",
    //     "unrealize",
    //     "visibility-notify-event",
    //     "window-state-event"
    // ]

    //     signals.forEach(event => {
    //         this.connect(event, () => {
    //             log(`${event} ${this.get_allocation().width}`);
    //         })
    //     })

    // }
});