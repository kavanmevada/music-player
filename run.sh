if [[ -f '_build' ]]; then
    meson _build
else
    meson setup --wipe _build
fi

sudo ninja -C _build install

com.kavan.MusicPlayer
